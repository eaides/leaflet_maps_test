@extends('layouts.app')

@section('css_before')
    <link href="{{  mix('/css/modules/home.css') }}" rel="stylesheet">
@endsection

@section('js_after_framework')
    {{--<script src="{{ asset('js/framework/masonry.pkgd.min.js') }}"></script>--}}
@endsection

@section('js_after')
    <script src="{{ mix('js/modules/home.js') }}" defer></script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <h3>This is the Home Page</h3>
    </div>
</div>
@endsection
