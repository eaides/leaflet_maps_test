@extends('layouts.app')

@section('css_after')
    <link href="{{  asset('css/leaflet.css') }}" rel="stylesheet">
    <link href="{{  mix('/css/modules/maps/events-groups.css') }}" rel="stylesheet">
@endsection

@section('js_after_framework')
    <script src="{{ asset('js/leaflet134/leaflet.js') }}"></script>
@endsection

@section('js_after')
    <script src="{{ mix('js/modules/maps/events-groups.js') }}"></script>
@endsection

@section('content')

    <h3>Events - Layer Groups</h3>

    <div id="main-leaflet-map"></div>

    <br>
@endsection
