@extends('layouts.app')

@section('css_after')
    <link href="{{  asset('css/leaflet.css') }}" rel="stylesheet">
{{--    <link href="{{  mix('/css/modules/maps/no/base.css') }}" rel="stylesheet">--}}
@endsection

@section('js_after_framework')
    <script src="{{ asset('js/leaflet134/leaflet.js') }}"></script>
@endsection

@section('js_after')
    <script src="{{ mix('js/modules/maps/no/base2.js') }}"></script>
@endsection

@section('content')

    <h3>Non Geographic Maps By Leaflet!</h3>

    <div id="main-leaflet-map"></div>

    <br><br>

    <input type="hidden" id="main-leaflet-map_url" value="{{ $data['map_path_internal'] }}">
    <input type="hidden" id="main-leaflet-map_width" value="{{ $data['width'] }}">
    <input type="hidden" id="main-leaflet-map_height" value="{{ $data['height'] }}">

    X = {{ $data['width'] }}
    Y = {{ $data['height'] }}

@endsection
