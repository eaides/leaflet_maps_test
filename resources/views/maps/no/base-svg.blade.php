@extends('layouts.app')

@section('css_after')
    <link href="{{  asset('css/leaflet.css') }}" rel="stylesheet">
{{--    <link href="{{  mix('/css/modules/maps/no/base.css') }}" rel="stylesheet">--}}
@endsection

@section('js_after_framework')
    <script src="{{ asset('js/leaflet134/leaflet.js') }}"></script>
@endsection

@section('js_after')
    <script src="{{ mix('js/modules/maps/no/base_svg.js') }}"></script>
@endsection

@section('content')

    <h3>Non Geographic SVG Maps By Leaflet!</h3>
    actual zoom: <span id="actual_zoom_span"></span>
    <div class="row">
        <div class="col-md-12">
            <div id="main-leaflet-map"></div>
        </div>
    </div>

    <br><br>

    <input type="hidden" id="main-leaflet-map_url" value="{{ $data['map_path_internal'] }}">
    <input type="hidden" id="main-leaflet-map_url_path" value="{{ $data['map_path'] }}">

@endsection
