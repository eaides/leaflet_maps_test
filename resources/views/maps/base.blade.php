@extends('layouts.app')

@section('css_after')
    <link href="{{  asset('css/leaflet.css') }}" rel="stylesheet">
    <link href="{{  mix('/css/modules/maps/base.css') }}" rel="stylesheet">
@endsection

@section('js_after_framework')
    <script src="{{ asset('js/leaflet134/leaflet.js') }}"></script>
@endsection

@section('js_after')
    <script src="{{ mix('js/modules/maps/base.js') }}"></script>
@endsection

@section('content')

    <h3>Hello, world!</h3>

    <i class="fas fa-user"></i> <!-- uses solid style -->
    <i class="far fa-user"></i> <!-- uses regular style -->
    <i class="fal fa-user"></i> <!-- uses light style -->
    <!--brand icon-->
    <i class="fab fa-github-square"></i> <!-- uses brands style --><br><br>

    <div id="main-leaflet-map"></div>

    <br><br>
@endsection
