<a class="navbar-brand" href="{{ url('/') }}">
    {{ config('app.name', 'Laravel') }}
</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
    <span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <!-- Left Side Of Navbar -->
    <ul class="navbar-nav mr-auto">
        <li class="nav-item {{ isActiveRoute('home') }}"><a class="nav-link" href="{{route('home')}}">Home</a></li>

        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle {{ areActiveRoutes(
                [
                'maps.base', 'maps.basic-markers', 'maps.events-groups',
                'maps.geojson',
                ]) }}" href="#" id="navbarDropdown"
               role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Geographic Maps</a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item {{ isActiveRoute('maps.base') }}" href="{{route('maps.base')}}">Base Map</a>
                <a class="dropdown-item {{ isActiveRoute('maps.basic-markers') }}" href="{{route('maps.basic-markers')}}">Basic Maps and Markers</a>
                <a class="dropdown-item {{ isActiveRoute('maps.events-groups') }}" href="{{route('maps.events-groups')}}">Events and Group Layers</a>
                <a class="dropdown-item {{ isActiveRoute('maps.geojson') }}" href="{{route('maps.geojson')}}">GeoJson 42</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle {{ areActiveRoutes(
                [
                'maps.no.base', 'maps.no.base2', 'maps.no.base.svg',
                ]) }}"
               href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Non Geographic Maps
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item {{ isActiveRoute('maps.no.base') }}" href="{{route('maps.no.base')}}">Base Map</a>
                <a class="dropdown-item {{ isActiveRoute('maps.no.base2') }}" href="{{route('maps.no.base2')}}">Base 2 Map Leaaflet</a>
                <a class="dropdown-item {{ isActiveRoute('maps.no.base.svg') }}" href="{{route('maps.no.base.svg')}}">Base SVG Map Leaaflet</a>
            </div>
        </li>
        {{--@guest--}}
        {{--<li class="nav-item {{ isActiveRoute('maps.basic-markers') }}"><a class="nav-link" href="{{route('maps.basic-markers')}}">Basic Maps and Markers</a></li>--}}
        {{--@else--}}
        {{--<li class="nav-item {{ isActiveRoute('maps.events-groups') }}"><a class="nav-link" href="{{route('maps.events-groups')}}">events-groups</a></li>--}}
        {{--@endguest--}}
    </ul>

    <!-- Right Side Of Navbar -->
    <ul class="navbar-nav ml-auto">
        <!-- Authentication Links -->
        @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            <li class="nav-item">
                @if (Route::has('register'))
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                @endif
            </li>
        @else
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        @endguest
    </ul>
</div>
