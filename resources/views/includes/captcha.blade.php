<div class="form-group">
    <div class="col-md-8 offset-md-1">
        <label id="captcha_url"><br><img src="{!! captcha_url() !!}" alt="captcha"></label>
        <button type="button" class="btn btn-sm float-right captcha-reload" id="captcha_reload"><i class="fas fa-sync-alt"></i></button>
    </div>
    <input type="text" class="form-control"
           id="captcha"
           name="captcha"
           placeholder="Número de verificación (captcha)"
           autocomplete="off"
    >
    @if ($errors->has('captcha'))
        <span class="form-text text-muted app-error">
                                        <strong>{{ $errors->first('captcha') }}</strong>
                                    </span>
        <input type="hidden" id="captcha_by_post" value="0">
    @else
        <input type="hidden" id="captcha_by_post" value="1">
    @endif
    <span class="form-text text-muted app-error"><strong style="display:none"  class="error_by_post" id="error_for_captcha"></strong></span>
</div>