@extends('errors::illustrated-layout')

@section('code', '503')
@section('title', __('Servicio No Disponible'))

@section('image')
<div style="background-image: url('/svg/503.svg');" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
</div>
@endsection

@section('message', __($exception->getMessage() ?: 'Disculpa, estamos haciendo mantenimientos. Por favor intenta más tarde.'))
