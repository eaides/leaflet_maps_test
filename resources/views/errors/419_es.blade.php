@extends('errors::illustrated-layout')

@section('code', '419')
@section('title', __('La Página Expiró'))

@section('image')
<div style="background-image: url('/svg/403.svg');" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
</div>
@endsection

@section('message', __('Disculpa, la sesión ha expirado. Por favor refresque la página e intente nuevamente.'))
