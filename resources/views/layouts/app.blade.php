@php
    $agent = new \Jenssegers\Agent\Agent();
    $platform = $agent->platform();
    $platform = preg_replace('/[[:space:]]+/', '-', $platform);
    $platform = strtolower($platform);
    $browser = $agent->browser();
    $browser_version = $agent->version($browser);
    $browser = preg_replace('/[[:space:]]+/', '-', $browser);
    $browser = strtolower($browser);
    $browser_version = preg_replace('/[[:space:]]+/', '-', $browser_version);
    $browser_version = strtolower($browser_version);
    $robot = $agent->robot();
    $robot = preg_replace('/[[:space:]]+/', '-', $robot);
    $robot = strtolower($robot);
    $body_class = ' class="app-is-unknown';
    $is_mobile = false;
    if ($agent->isMobile() || $agent->isTablet()) {
        if($agent->isPhone()) {
            $body_class = ' class="app-is-mobile app-is-phone';
        } else {
            $body_class = ' class="app-is-mobile';
        }
        $is_mobile = true;
    } else if($agent->isDesktop()) {
        $body_class = ' class="app-is-desktop';
    } else if($agent->isRobot()) {
        $body_class = ' class="app-is-robot robot-' . $robot;
    }
    $body_class .= ' platform-' . $platform . ' browser-' . $browser . ' browser-version-' . $browser_version . '"';
@endphp
<!DOCTYPE html>
@if (!$is_mobile)
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@else
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" style="height: 100%">
@endif
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    @if (!$is_mobile)
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @else
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0">
    @endif

        <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- TITLE -->
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    @yield('css_before')
    <link href="{{  mix('/css/app.css') }}" rel="stylesheet">
    @yield('css_after')

    <script>
        var baseUrl = "{{ url('/') }}";
    </script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{ asset('js/framework/html5shiv_373/html5shiv.min.js') }}"></script>
    <script src="{{ asset('js/framework/respond_142/respond.min.js') }}"></script>
    <![endif]-->

</head>
<body{!! $body_class !!}>
    <div id="app"class="main_div_app @yield('main_extra_class','')">

        @include('includes.header')

        <main role="main" class="container app-ppal-div">
            {{--<div id="the-main-conatiner-app-div">--}}
                @yield('content')
            {{--</div>--}}
        </main>

        @include('includes.footer')
    </div>

    <!-- Scripts Frameworks-->
    @yield('js_before_framework')
    <script src="{{ asset('js/framework/jquery-3.3.1.min.js') }}"></script>
    @yield('js_after_jquery')
    <script src="{{ asset('js/framework/popper.js') }}"></script>
    <script src="{{ asset('js/framework/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/framework/all.min.js') }}"></script>
    {{--<script src="{{ asset('js/framework/lightbox.min.js') }}"></script>--}}
    <script src="{{ asset('js/framework/sweetalert2.min.js') }}"></script>
    @yield('js_after_framework')

    <!-- Scripts Frameworks-->
    @yield('js_before')
    <script src="{{ mix('js/app.js') }}" defer></script>
    @yield('js_after')

</body>
</html>
