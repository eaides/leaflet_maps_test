(function($) {

    $(document).ready(function() {
        lightbox.option({
            'resizeDuration': 200,
            'wrapAround': true,
            'alwaysShowNavOnTouchDevices': true,
            'albumLabel': "Foto %1 de %2"
        })
    });

})( jQuery );