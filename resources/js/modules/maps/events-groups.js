(function($) {
    $(document).ready(function() {

        var mymap;
        var lastMarker = false;

        var createMap = function() {

            // mymap = L.map('mapid').setView([51.505, -0.09], 13);
            mymap = L.map('main-leaflet-map',{
                center: [35.10418, -106.62987],
                zoom: 10
            })

            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                maxZoom: 18,
                id: 'mapbox.streets',
                accessToken: 'pk.eyJ1IjoiZWFpZGVzIiwiYSI6ImNqbnRhNWY4ODByc2g0MXBhNzIwNnk5aTEifQ.i9zWaPi_N4Dslv08TJoqww'
            }).addTo(mymap);


            // var myCircle = L.circle([35.10418, -106.62987], {
            //   color: 'red',
            //   fillColor: '#f03',
            //   fillOpacity: 0.5,
            //   radius: 500
            // }).addTo(mymap);

            var myMarker = L.marker([35.12, -106.63],
                {title:"My Point", alt:"the big I1",draggable:true}
            );
            var myMarker2 = L.marker([35.13, -106.64],
                {title:"My 2 Point", alt:"the big I2",draggable:true}
            );
            var myMarker3 = L.marker([35.14, -106.65],
                {title:"My 3 Point", alt:"the big I3",draggable:true}
            );
            var myMarker4 = L.marker([35.15, -106.66],
                {title:"My 4 Point", alt:"the big I4",draggable:true}
            );

            // bindpopup don't work on layersgroups
            var myLayerGroup = L.layerGroup([myMarker, myMarker3]).addTo(mymap).bindPopup('hello popup layer!');
            myLayerGroup.addLayer(myMarker2);
            myLayerGroup.addLayer(myMarker4);


            // bindpopup yes work on layersgroups
            var myMarker5 = L.marker([35.17, -106.67],
                {title:"My 5 Point", alt:"the big I4",draggable:true}
            ).addTo(mymap).bindPopup('hello popup point!<br>2 lines! and <b>html</b> suppported!');

            mymap.on('click', function(e) {
                var cord=e.latlng.toString().split(',');
                var lat = cord[0].split('(');
                var long = cord[1].split(')');
                // alert("you click on LAT: "+lat[1]+" and LONG: "+long[0]);
                swal({
                    title: "New Point",
                    text: "you click on LAT: "+lat[1]+" and LONG: "+long[0],
                    type: "info"
                });
                console.log(e.latlng);
                console.log(lat);
                console.log(long);
                if (lastMarker!==false){
                    lastMarker.remove();
                }
                lastMarker = L.marker(e.latlng, {title:"The New", alt:"the big new", draggable:true}).addTo(mymap);
            });
        }


        createMap();

    });
})(jQuery);