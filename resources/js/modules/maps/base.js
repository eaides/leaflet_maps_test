(function($) {

    $(document).ready(function() {

        // // test sweet alert only test
        // swal({
        //     title: "Base Map",
        //     text: "This is a base map test",
        //     type: "info"
        // });

        var mymap;

        var createMap = function() {

            // mymap = L.map('mapid').setView([51.505, -0.09], 13);
            mymap = L.map('main-leaflet-map',{
                center: [35.10418, -106.62987],
                zoom: 10
            })

            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                maxZoom: 18,
                id: 'mapbox.streets',
                accessToken: 'pk.eyJ1IjoiZWFpZGVzIiwiYSI6ImNqbnRhNWY4ODByc2g0MXBhNzIwNnk5aTEifQ.i9zWaPi_N4Dslv08TJoqww'
            }).addTo(mymap);

        }

        createMap();

    });

})( jQuery );