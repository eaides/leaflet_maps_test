(function($) {

    $(document).ready(function() {

        var lastMarker = false;
        var width = $('#main-leaflet-map_width').val();
        var height = $('#main-leaflet-map_height').val();
        var url = $('#main-leaflet-map_url').val();
        var height2 = height / 2;
        var width2 = width / 2;
        height2 = parseInt(height2);
        width2 = parseInt(width2);
        $('#main-leaflet-map').css('width',width2+'px');
        $('#main-leaflet-map').css('height',height2+'px');

        var mymap;

        var createMap = function() {

            mymap = L.map('main-leaflet-map',{
                crs: L.CRS.Simple,
                minZoom: -1
            });

            var bounds = [[0,0],[height,width]];
            var image = L.imageOverlay(url, bounds, {
                attribution: 'non geographic map test 2'
            }).addTo(mymap);
            mymap.fitBounds(bounds);
            mymap.setMaxBounds(bounds);
        }

        createMap();

        var marker1 = xy(920,670);
        L.marker(marker1).addTo(mymap).bindPopup('marker 1 x: 920 y: 670');
        mymap.setView([670,920],1);

        var pos1 = xy(200,100);
        var pos2 = xy(200,300);
        var pos3 = xy(400,200);
        var travel = L.polyline([pos1,pos2,pos3,pos1]).addTo(mymap);

        mymap.on('click', function(e) {
            var cord=e.latlng.toString().split(',');
            var lat = cord[0].split('(');
            var long = cord[1].split(')');
            // alert("you click on LAT: "+lat[1]+" and LONG: "+long[0]);
            var is_new = "New Point";
            if (lastMarker!==false){
                is_new = "Move the New Point";
            }
            swal({
                title: is_new,
                text: "you click on X: "+long[0] + " and Y: "+lat[1],
                type: "info"
            });
            console.log(e.latlng);
            console.log(lat);
            console.log(long);
            if (lastMarker!==false){
                lastMarker.remove();
            }
            lastMarker = L.marker(e.latlng, {title:"The New", alt:"the new point", draggable:true}).addTo(mymap);
        });

    });

    var yx = L.latLng;
    var xy = function(x, y) {
        if (L.Util.isArray(x)) {
            return yx(x[1], x[0]);  // using like xy([x, y]);
        }
        return yx(y, x);    // using like xy(x, y);
    }
})( jQuery );