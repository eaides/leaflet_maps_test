(function($) {

    $(document).ready(function() {

        // $('#the-main-conatiner-app-div').removeClass('container');
        // $('#the-main-conatiner-app-div').addClass('container-fluid');
        var actualZoom = $('#actual_zoom_span');

        var lastMarker = false;
        var url = $('#main-leaflet-map_url').val();
        var url_path = $('#main-leaflet-map_url_path').val();
        var mymap;
        $('#main-leaflet-map').css('height','480px');

        var createMap = function() {

            mymap = L.map('main-leaflet-map',{
                crs: L.CRS.Simple,
                minZoom: -3,
                maxZoom: 3,
                zoom: -2
            });
            do_map_on_events(mymap, actualZoom);

            var bounds = [[-100,-100],[2148,2148]];
            // var bounds = [[40.712216, -74.22655], [40.773941, -74.12544]];
            var image = L.imageOverlay(url_path, bounds, {
                attribution: 'non geographic SVG map test'
            }).addTo(mymap);
            mymap.fitBounds(bounds);
            mymap.setMaxBounds(bounds);
            $(actualZoom).html(mymap.getZoom());
        }

        createMap();

        var marker1 = xy(920,670);
        L.marker(marker1).addTo(mymap).bindPopup('marker 1 x: 920 y: 670');
        mymap.setView([670,920]);   // set view point without change zoom
        // mymap.setView([670,920],-1);   // set view point without change zoom

        var pos1 = xy(200,100);
        var pos2 = xy(200,300);
        var pos3 = xy(400,200);
        var travel = L.polyline([pos1,pos2,pos3,pos1]).addTo(mymap);


    });

    var yx = L.latLng;
    var xy = function(x, y) {
        if (L.Util.isArray(x)) {
            return yx(x[1], x[0]);  // using like xy([x, y]);
        }
        return yx(y, x);    // using like xy(x, y);
    }

    var do_map_on_events = function(mymap, actualZoom) {

        mymap.on('click', function(e) {
            var cord=e.latlng.toString().split(',');
            var lat = cord[0].split('(');
            var long = cord[1].split(')');
            // alert("you click on LAT: "+lat[1]+" and LONG: "+long[0]);
            var is_new = "New Point";
            if (lastMarker!==false){
                is_new = "Move the New Point";
            }
            swal({
                title: is_new,
                text: "you click on X: "+long[0] + " and Y: "+lat[1],
                type: "info"
            });
            console.log(e.latlng);
            console.log(lat);
            console.log(long);
            if (lastMarker!==false){
                lastMarker.remove();
            }
            lastMarker = L.marker(e.latlng, {title:"The New", alt:"the new point", draggable:true}).addTo(mymap);
        });

        mymap.on('zoomend', function() {
            $(actualZoom).html(mymap.getZoom());
        });

        // mymap.on('map-container-resize', function() {
        //    mymap.invalidateSize();
        // });

        mymap.on('load', function() {
            // console.log('to set zoom  = -2');
            mymap.setZoom(-2);
        });

    }

})( jQuery );