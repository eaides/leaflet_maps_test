(function($) {

    $(document).ready(function() {

        var width = $('#main-leaflet-map_width').val();
        var height = $('#main-leaflet-map_height').val();
        var url = $('#main-leaflet-map_url').val();
        $('#main-leaflet-map').css('width',width+'px');
        $('#main-leaflet-map').css('height',height+'px');

        var mymap;

        var createMap = function() {

            mymap = L.map('main-leaflet-map',{
                crs: L.CRS.Simple
            });

            var bounds = [[0,0],[height,width]];
            var image = L.imageOverlay(url, bounds, {
                attribution: 'non geographic map test'
            }).addTo(mymap);
            mymap.fitBounds(bounds);
            mymap.setMaxBounds(bounds);
        }

        createMap();

    });

})( jQuery );