(function($) {
    $(document).ready(function() {

        var mymap;
        var lastMarker = false;

        var geoJsonLayer1;
        var geoJsonLayer2;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function newStyle() {
            geoJsonLayer1.setStyle(
                {
                    color:"green"
                }
            );
        }

        function styleFunction(feature) {
            // console.log(feature);
            switch (feature.geometry.type) {
                case 'LineString':
                    return {color: "red"};
                    break;
                case 'Polygon':
                    return {color: "purple"};
                    break;
            }
        }

        function onEachFeature(feature, layer) {
            // does this feature have a property named popupContent?
            if (feature.properties && feature.properties.popupContent) {
                console.log(layer);
                console.log(feature);
                layer.bindPopup(feature.properties.popupContent);
            }
        }

        var createMap = function(data) {

            console.log(data);
            var geojson = data;

            mymap = L.map('main-leaflet-map',{
                center: [35.10418, -106.62987],
                zoom: 10
            });

            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                maxZoom: 18,
                id: 'mapbox.streets',
                accessToken: 'pk.eyJ1IjoiZWFpZGVzIiwiYSI6ImNqbnRhNWY4ODByc2g0MXBhNzIwNnk5aTEifQ.i9zWaPi_N4Dslv08TJoqww'
            }).addTo(mymap);

            geoJsonLayer1 = L.geoJSON(geojson, {
                style: styleFunction,
                onEachFeature: onEachFeature
            }).addTo(mymap);

            // geoJsonLayer1.on('mouseover', newStyle);
            // geoJsonLayer1.on('mouseout', function(e) {
            //     geoJsonLayer1.resetStyle(e.target);
            // });

        };

        $.ajax({
            url: 'geojson/get/data',
            type: 'POST',
            data: {
            },
            beforeSend: function() {

            },
            success: function(data) {
                createMap(data);
            },
            complete: function(){

            }
        });

    });
})(jQuery);