(function($) {
    $(document).ready(function() {

        // console.log('document ready by jQuery using $');

        var captchaReloadUrl = baseUrl + '/photo-captcha-reload';
        captcha_reload = function() {
            // setup ajax with csrf token
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // call re-captcha by ajax
            $.ajax({
                url: captchaReloadUrl,
                type: 'POST',
                data: {
                },
                beforeSend: function() {
                    $("#captcha_reload").prop('disabled', true);
                },
                success: function(data) {
                    $('#captcha_url img').attr('src',data.captcha);
                },
                complete: function(){
                    $("#captcha_reload").prop('disabled', false);
                }
            })
        };

        $(document).on('click', '#captcha_reload', function() {
            captcha_reload();
        });

    });
})( jQuery );