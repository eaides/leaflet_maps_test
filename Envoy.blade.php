@include('vendor/autoload.php')

@setup
    $origin = 'git@bitbucket.org:eaides/laravel-leaflet.git';
    $branch = isset($branch) ? $branch : 'master';
    $www_dir = '/var/www/html';
    $app_dir = '/var/www/html/laravel-leaflet';
    if (!isset($on)) {
        throw new Exception('The variable --on is not defined');
    }
    (new Dotenv\Dotenv(__DIR__, '.env'))->load();
    $aws_ip = getenv('AWS_IP');
    if (!$aws_ip) {
        throw new Exception('The environment variable AWS_IP is not defined');
    }
    $aws_connection = '-i /home/vagrant/curso-envoy-aws.pem ubuntu@' . $aws_ip;
@endsetup

@servers(['aws'=>$aws_connection])

@task('git:clone', ['on' => $on])
    cd {{ $www_dir }}
    echo "inside directory: {{ $www_dir }}";
    git clone {{ $origin }}
    echo 'repository cloned successfully';
@endtask

@task('ls', ['on' => $on])
    cd {{ $app_dir }}
    echo "listing directory: {{ $app_dir }}";
    ls -la
@endtask

@task('composer:install', ['on' => $on])
    cd {{ $app_dir }}
    echo "composer install on directory: {{ $app_dir }}";
    composer install
@endtask

@task('key:generate', ['on' => $on])
    cd {{ $app_dir }}
    echo "key generate on directory: {{ $app_dir }}";
    php artisan key:generate
@endtask

@task('git:pull', ['on' => $on])
    cd {{ $app_dir }}
    echo "inside directory: {{ $app_dir }}";
    git pull origin {{ $branch }}
    echo 'code updated successfully';
@endtask

@task('migrate', ['on' => $on])
    cd {{ $app_dir }}
    echo "inside directory: {{ $app_dir }}";
    php artisan migrate --force
    echo 'database migrated';
@endtask

@task('yarn:install', ['on' => $on])
    cd {{ $app_dir }}
    echo "inside directory: {{ $app_dir }}";
    yarn install
    echo 'yarn executed';
@endtask





@task('update:project', ['on' => $on])
    cd {{ $app_dir }}
    echo "inside directory: {{ $app_dir }}";
    php artisan down
        rm -f package-lock.json
        rm -f yarn.lock
        mkdir -p public/backgrounds
        mkdir -p storage/app/backgrounds
        git pull origin {{ $branch }}
        echo 'code updated successfully';
    php artisan view:clear
    php artisan cache:clear
    php artisan config:clear
    php artisan optimize:clear
    php artisan route:clear
    echo 'all caches clear';
        composer install
        echo 'vendor composer installed successfully';
    rm -f css/app.css
    rm -f js/app.js
    npm run prod
    echo 'mix generated successfully';
        php artisan migrate --force
        echo 'database migrated';
    php artisan up
    php artisan queue:restart
@endtask
