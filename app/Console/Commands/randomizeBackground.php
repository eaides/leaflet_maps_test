<?php

namespace App\Console\Commands;

use App\LastUsedBackground;
use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Mockery\Exception;

class randomizeBackground extends Command
{
    protected $min_seconds = 900;  // 15 minutes = 900 seconds -- cron runs every minute

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'randomize:background';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'select a new random file for background';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws FileNotFoundException
     */
    public function handle()
    {
        $mainBackgroundsFolders = getMainBackgroundsFolder();
        if (!$mainBackgroundsFolders) return false;

        // read all backgrounds pictures
        $all_files = Storage::disk('local')->allFiles($mainBackgroundsFolders);
        if (!count($all_files)) return false;

        $min_seconds = $this->min_seconds;
        $forceChange = false;
        $lastDBBackground = LastUsedBackground::find(1);
        if (!$lastDBBackground) {
            $lastDBBackground = new LastUsedBackground();
            $lastDBBackground->id = 1;
            $lastDBBackground->date_and_time = now()->format('Y-m-d H:i:s');
            $lastDBBackground->last1 = '';
            $lastDBBackground->last2 = '';
            $lastDBBackground->last3 = '';
            $lastDBBackground->save();
            $forceChange = true;
        }
        $last_time = $lastDBBackground->date_and_time;
        $last_time = Carbon::parse($last_time);
        $diff_time = now()->diffInSeconds($last_time);
        if ($diff_time > $min_seconds || $forceChange) {
            $last1 = $lastDBBackground->last1;
            $last2 = $lastDBBackground->last2;
            $last3 = $lastDBBackground->last3;

            // first save now as last updated background pictures
            $lastDBBackground->date_and_time = now()->format('Y-m-d H:i:s');
            $lastDBBackground->save();

            $try = 0;
            $do_rand = true;
            while ($do_rand) {
                $try++;
                $index = mt_rand(0, count($all_files) - 1);
                $rnd_file_name_full = $all_files[$index];
                $rnd_file_name_parts = pathinfo($rnd_file_name_full);
                // get a random filename
                $rnd_file_name = $rnd_file_name_parts['basename'];
                $do_save = false;
                if ($rnd_file_name != $last1 && $rnd_file_name != $last2 && $rnd_file_name != $last3) {
                    // if was not used 3 times ago, select it as background
                    $do_save = true;
                }
                if ($do_save) {
                    // save random file as  background
                    $dest_name = 'background.' . $rnd_file_name_parts['extension'];
                    Storage::disk($mainBackgroundsFolders)->put($dest_name, Storage::disk('local')->get($rnd_file_name_full));
                    // remember as last used
                    $lastDBBackground->last3 = $lastDBBackground->last2;
                    $lastDBBackground->last2 = $lastDBBackground->last1;
                    $lastDBBackground->last1 = $rnd_file_name;
                    $lastDBBackground->save();
                    // exit loop
                    $do_rand = false;
                }
                // try only 25 times
                if ($try > 25) $do_rand = false;
            }
        }
        return true;
    }
}
