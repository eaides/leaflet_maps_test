<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PhotoController extends Controller
{

    /**
     * @param string $type
     * @return \Illuminate\Http\JsonResponse
     */
    public function captchaReload($type = 'default')
    {
        return response()->json(['captcha' => captcha_url()]);
    }

}
