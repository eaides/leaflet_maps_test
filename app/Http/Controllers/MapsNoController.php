<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MapsNoController extends Controller
{
    protected $max_w = 800;
    protected $max_h = 600;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function mapsBase()
    {
        $file_name = '00000001/floor1.jpg';
        $map_path_internal = Storage::url($file_name);
        $map_path = asset('storage/' . $file_name);
        if (!Storage::disk('public')->has($file_name)) {
            abort(500);
        }
        // /home/vagrant/Code/leaflet_maps_test/public/storage/00000001/floor1.jpg
        $file_data = getimagesize($map_path);
        if (is_array($file_data) && array_key_exists('mime', $file_data)) {
            $mime = $file_data['mime'];
            $mime = strtolower($mime);
            $width = $file_data[0];
            $height = $file_data[1];
            if ($width > $this->max_w || $height > $this->max_h) {
                $ratio_w = $width / $this->max_w;
                $ratio_h = $height / $this->max_h;
                if ($ratio_w >= $ratio_h) $ratio = $ratio_w;
                else $ratio = $ratio_h;
                $width = $width / $ratio;
                $height = $height / $ratio;
            }
            if ($mime!='image/jpeg') {
                abort(500);
            }
            $data = [
                'map_path' => $map_path,
                'width' => $width,
                'height' => $height,
                'map_path_internal' => ltrim($map_path_internal,'/'),
            ];
            return view('maps.no.base')->with(compact('data'));
        }
        abort(500);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function mapsBase2()
    {
        // /home/vagrant/Code/leaflet_maps_test/public/storage/00000001/floor1.jpg
        $map_path_internal = '/storage/00000001/floor1.jpg';    //  Storage::url('00000001/floor1.jpg');
        $map_path = asset('storage/00000001/floor1.jpg');
        $file_data = getimagesize($map_path);
        if (is_array($file_data) && array_key_exists('mime', $file_data)) {
            $mime = $file_data['mime'];
            $mime = strtolower($mime);
            $width = $file_data[0];
            $height = $file_data[1];
            if ($mime!='image/jpeg') {
                abort(500);
            }
            $data = [
                'map_path' => $map_path,
                'width' => $width*2,
                'height' => $height*2,
                'map_path_internal' => $map_path_internal, //ltrim($map_path_internal,'/'),
            ];
            return view('maps.no.base2')->with(compact('data'));
        }
        abort(500);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function mapsBaseSvg()
    {
        $map_path_internal = '/storage/00000002/floor-1.svg';
        $map_path = asset('storage/00000002/floor-1.svg');
        // $file_data = getimagesize($map_path);
        $mime_content_type = mime_content_type(ltrim($map_path_internal,'/'));
        $mime_content_type = strtolower($mime_content_type);
        if (substr($mime_content_type,0,9)=='image/svg') {
            $data = [
                'map_path' => $map_path,
                'map_path_internal' => $map_path_internal,
            ];
            return view('maps.no.base-svg')->with(compact('data'));

        }
        abort(500);
    }

}
