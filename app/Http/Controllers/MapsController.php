<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MapsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function mapsBase()
    {
        $data = [];
        return view('maps.base')->with(compact('data'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function mapsBasicMarkers()
    {
        $data = [];
        return view('maps.basic-markers')->with(compact('data'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function mapsEventsGroups()
    {
        $data = [];
        return view('maps.events-groups')->with(compact('data'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function mapsGeoJson()
    {
        $data = [];
        return view('maps.geojson')->with(compact('data'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function mapsGeoJsonGetData() {
        $geoJson0 = [
            "type" => "FeatureCollection",
            "features" => [
                [
                    "type" => "Feature",
                    "geometry" => [
                        "type" => "Point",
                        "coordinates" => [-106.62987,35.10418],
                    ],
                    "properties" => [
                        "name" => "My Point",
                        "title" => "A point at the Big I"
                    ],
                ],
                [
                    "type" => "Feature",
                    "geometry" => [
                        "type" => "Point",
                        "coordinates" => [-106,35],
                    ],
                    "properties" => [
                        "name" => "My Other Point",
                        "title" => "A point near Moriarty and I40"
                    ],
                ],
            ],
        ];

        $geoJson1 = [
            [
                "type" => "Feature",
                "geometry" => [
                    "type" => "Point",
                    "coordinates" => [-106.62987,35.10418],
                ],
                "properties" => [
                    "name" => "My Point",
                    "title" => "A point at the Big I"
                ],
            ],
            [
                "type" => "Feature",
                "geometry" => [
                    "type" => "Point",
                    "coordinates" => [-106,35],
                ],
                "properties" => [
                    "name" => "My Other Point",
                    "title" => "A point near Moriarty and I40"
                ],
            ],
        ];

        $geoJson2 = [
            [
                "type" =>  "Feature",
                "geometry" =>  [
                    "type" =>  "Point",
                    "coordinates" =>  [-106.62987, 35.10418]
                ],
                "properties" =>  [
                    "name" =>  "My Point",
                    "title" =>  "A point at the Big I",
                    "amenity" => "A point at the Big I",
                    "popupContent" => "POPUP : A point at the Big I"
                ]
            ],
            [
                "type" =>  "Feature",
                "geometry" =>  [
                    "type" =>  "LineString",
                    "coordinates" => [
                        [-106.67999, 35.14097],
                        [-106.68892, 35.12974],
                        [-106.69064, 35.1098]
                    ]
                ],
                "properties" =>  [
                    "name" =>  "My LineString",
                    "title" =>  "A line along the Rio Grande",
                    "amenity" => "A line along the Rio Grande",
                    "popupContent" => "POPUP : A line along the Rio Grande"
                ]
            ],
            [
                "type" =>  "Feature",
                "geometry" =>  [
                    "type" =>  "Polygon",
                    "coordinates" => [
                        [
                            [-106.78059, 35.14574],
                            [-106.7799, 35.10559],
                            [-106.71467, 35.13704],
                            [-106.69716, 35.17942],
                            [-106.78059, 35.14574]
                        ]
                    ]
                ],
                "properties" =>  [
                    "name" =>  "My Polygon",
                    "title" =>  "Balloon Fiesta Park",
                    "amenity" => "Balloon Fiesta Park",
                    "popupContent" => "POPUP : Balloon Fiesta Park"
                ]
            ]
        ];

        $geoJson3 = [
            [
                "type" =>  "Feature",
                "geometry" =>  [
                    "type" =>  "Polygon",
                    "coordinates" => [
                        [
                            [-106.875, 35.20074],
                            [-106.82281, 34.9895],
                            [-106.50146, 35.00525],
                            [-106.47949, 35.1985],
                            [-106.875, 35.20074]
                        ],
                        [
                            [-106.6745, 35.1463],
                            [-106.70403, 35.05192],
                            [-106.55296, 35.05979],
                            [-106.53854, 35.17212],
                            [-106.6745, 35.1463]
                        ]
                    ]
                ],
                "properties" =>  [
                    "name" =>  "My Polygon with a hole",
                    "title" =>  "Donut"
                ]
            ]
        ];

        return response()->json($geoJson2);
    }
}
