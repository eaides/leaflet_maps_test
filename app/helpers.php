<?php

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Detect Active Route
|--------------------------------------------------------------------------
|
| Compare given route with current route and return output if they match.
| Very useful for navigation, marking if the link is active.
|
*/
if (!function_exists('isActiveRoute')) {
    function isActiveRoute($route, $output = "active")
    {
        if (Route::currentRouteName() == $route) return $output;
    }
}

/*
|--------------------------------------------------------------------------
| Detect Active Routes
|--------------------------------------------------------------------------
|
| Compare given routes with current route and return output if they match.
| Very useful for navigation, marking if the link is active.
|
*/
if (!function_exists('areActiveRoutes')) {
    function areActiveRoutes(Array $routes, $output = "active")
    {
        foreach ($routes as $route)
        {
            if (Route::currentRouteName() == $route) return $output;
        }
    }
}

if (!function_exists('adminEmail')) {
    function adminEmail()
    {
        $admin = 'eaides@hotmail.com';
        return $admin;
    }
}

if (!function_exists('getMainBackgroundsFolder')) {
    function getMainBackgroundsFolder()
    {
        // return false if not use backgrounds
        $backgroundsFolders = 'backgrounds';
        // check if exists or create folder
        $folderExists = true;
        if (!File::exists(public_path($backgroundsFolders))) {
            try {
                $folderExists = File::makeDirectory(public_path($backgroundsFolders), 0557, true);
            } catch(Exception $e) {
                return false;
            }
        }
        if (!$folderExists) return false;
        return $backgroundsFolders;
    }
}