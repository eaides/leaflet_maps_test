<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LastUsedBackground extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','last1', 'last2', 'last3','date_and_time',
    ];

}
