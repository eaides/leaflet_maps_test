<?php

use App\LastUsedBackground;
use Illuminate\Database\Seeder;

class LastUsedBackgroundSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!LastUsedBackground::find(1)) {
            try {
                factory(LastUsedBackground::class)->create([
                    'id' => 1,
                ]);
            } catch (\Illuminate\Database\QueryException $exception) {
                echo 'error when create';
            }
        }
    }
}
