<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!User::find(1)) {
            try {
                factory(User::class)->create([
                    'id' => 1,
                    'name' => 'Ernesto Aides',
                    'email' => 'eaides@hotmail.com',
                    'password' => bcrypt('ena2101'),
                    'remember_token' => '',

                ]);
            } catch (\Illuminate\Database\QueryException $exception) {
                echo 'error when create';
            }
        }
    }
}
