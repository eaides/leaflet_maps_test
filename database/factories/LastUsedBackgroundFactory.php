<?php

use Faker\Generator as Faker;

$factory->define(App\LastUsedBackground::class, function (Faker $faker) {
    return [
        'id' => $faker->numberBetween(1,1000),
        'last1' => $faker->uuid.'.jpg',
        'last2' => $faker->uuid.'.jpg',
        'last3' => $faker->uuid.'.jpg',
        'date_and_time' => $faker->dateTime,
    ];
});
