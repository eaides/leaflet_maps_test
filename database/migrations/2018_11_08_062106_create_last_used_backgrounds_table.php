<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLastUsedBackgroundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('last_used_backgrounds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('last1', 250)->default('');
            $table->string('last2', 250)->default('');
            $table->string('last3', 250)->default('');
            $table->dateTime('date_and_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('last_used_backgrounds');
    }
}
