<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', 'HomeController@index')->name('root');
Route::get('/home', 'HomeController@index')->name('home');

Route::post('photo-captcha-reload', 'PhotoController@captchaReload');

Route::middleware(['auth'])->group(function () {

});

Route::middleware(['guest'])->group(function () {

});

Route::get('/maps-base', 'MapsController@mapsBase')->name('maps.base');
Route::get('/basic-markers', 'MapsController@mapsBasicMarkers')->name('maps.basic-markers');
Route::get('/events-groups', 'MapsController@mapsEventsGroups')->name('maps.events-groups');
Route::get('/geojson', 'MapsController@mapsGeoJson')->name('maps.geojson');
Route::post('/geojson/get/data', 'MapsController@mapsGeoJsonGetData');

Route::get('/maps-no-base', 'MapsNoController@mapsBase')->name('maps.no.base');
Route::get('/maps-no-base-2', 'MapsNoController@mapsBase2')->name('maps.no.base2');
Route::get('/maps-no-base-svg', 'MapsNoController@mapsBaseSvg')->name('maps.no.base.svg');
