const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//    .sass('resources/sass/app.scss', 'public/css');

mix.styles([
    'resources/css/bootstrap.min.css',
    'resources/css/sweetalert2.min.css',
    'resources/css/all.min.css',
    // 'resources/css/lightbox.min.css',
    'resources/css/app.css'
], 'public/css/app.css');

mix.styles([
    'resources/css/modules/home.css'
], 'public/css/modules/home.css');

mix.styles([
    'resources/css/modules/maps/base.css'
], 'public/css/modules/maps/base.css');

mix.styles([
    'resources/css/modules/maps/basic-markers.css'
], 'public/css/modules/maps/basic-markers.css');

mix.styles([
    'resources/css/modules/maps/events-groups.css'
], 'public/css/modules/maps/events-groups.css');

mix.styles([
    'resources/css/modules/maps/geojson.css'
], 'public/css/modules/maps/geojson.css');

mix.js([
    'resources/js/app.js'
], 'public/js/app.js');

mix.js([
    'resources/js/modules/home.js'
], 'public/js/modules/home.js');

mix.js([
    'resources/js/modules/maps/base.js'
], 'public/js/modules/maps/base.js');

mix.js([
    'resources/js/modules/maps/basic-markers.js'
], 'public/js/modules/maps/basic-markers.js');

mix.js([
    'resources/js/modules/maps/events-groups.js'
], 'public/js/modules/maps/events-groups.js');

mix.js([
    'resources/js/modules/maps/geojson.js'
], 'public/js/modules/maps/geojson.js');

mix.js([
    'resources/js/modules/maps/no/base.js'
], 'public/js/modules/maps/no/base.js');

mix.js([
    'resources/js/modules/maps/no/base2.js'
], 'public/js/modules/maps/no/base2.js');

mix.js([
    'resources/js/modules/maps/no/base_svg.js'
], 'public/js/modules/maps/no/base_svg.js');

// if (mix.inProduction()) {
mix.version();
//}